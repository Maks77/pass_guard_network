import Vue from 'vue';
import axios from 'axios';

import api from "./appRoutes";

// http://storage.nova-solutio.com/api/user/login
// POST
// login root
// password 5452050REdo

let Search = {
	in(arr) {
		this.data = arr;
		return this;
	},
	getProject(proj_id) {
		return this.data.find(el=>{
			return el.id == proj_id;
		})
	},
	getService(proj_id, serv_id) {
		let project = this.getProject(proj_id);
		if (project == undefined) {
			return undefined
		}
		return project.groups.find(el=>{
			return el.id == serv_id;
		})
	}
};

let main = new Vue({
	data() {
		return {
			mainData: 	[],
			colors: 	[],
			icons: 		[],
			online: true,
			access_header: {}
		}
	},

	created() {
		this.checkOnlineStatus();

		this.$on('onMainUpdate', () => {
			this.cacheData()
		})
		this.$on('onColorLoaded', () => {
			this.cacheData()
		})
		this.$on('onIconsLoaded', () => {
			this.cacheData()
		})

		this.$on('getToken', (head) => {
			this.updateData(head);
			this.getColors();
			this.getIcons();
		})

	},

	methods: {
		
		checkOnlineStatus() { 				//проверка наличия соединения с обновлением данных
			// if (navigator.onLine) {
			if (navigator.connection.type !== 'none') {
				this.online = true;
				this.updateData();
				this.getColors();
				this.getIcons();
			} else {
				this.online = false;
				this.getDataFromCache();
	
				console.log('get data from localstorage')
			}
			this.$emit('onConnectUpdate', this.online) // проверка наличия соединения для отображения/скрытия возможности редактирования (for: [panel-left, project])
		},

		
		simpleConnectionCheck() { 			//проверка наличия соединения без обновления данных
			// if (navigator.onLine) {
			if (navigator.connection.type !== 'none') {
				this.online = true;
			} else {
				this.online = false;
				this.$f7.dialog.alert('Отсутствует интернет соединение', 'Внимание', () => {});
			}
			this.$emit('onConnectUpdate', this.online)
		},
		
		updateData(incomingHeader) {
			console.log('.....updating data')
			this.simpleConnectionCheck();

			let headerObj = incomingHeader;
			if (headerObj == undefined) {
				headerObj = {
					headers: {
						"Authorization": `Bearer ${localStorage.getItem('access_token')}`
					}
				}
			}
			this.access_header = headerObj;

			axios
				.get(api.project.index, this.access_header)
				.then(response => {
					this.mainData = response.data;
					this.$emit('onMainUpdate', this.mainData)
					console.log('.....data was updated')
				})
				.catch(function (error) {
					console.dir(error);
					if (error.message == 'Network Error') {
						this.simpleConnectionCheck();
					}
				});
		},

		getAccessToken(userData) {
			let params = setUrlSearchParams(userData);

			axios
				.post(api.user.login, params)
				.then(response=>{
					let access_token = response.data.access_token;
					if (access_token == undefined) {
						this.$emit('onLogIn', false)
						return;
					}
					localStorage.setItem('access_token', access_token)

					let header = {headers: {"Authorization": `Bearer ${access_token}`}}
					
					this.$emit('getToken', header)
					this.$emit('onLogIn', true)
				})
				.catch(e=>{console.log(e)})
		},

		cacheData() {
			let localMainData = JSON.stringify(this.mainData)
			let localColors = JSON.stringify(this.colors)
			let localIcons = JSON.stringify(this.icons)

			localStorage.setItem('localMainData', localMainData)
			localStorage.setItem('localColors', localColors)
			localStorage.setItem('localIcons', localIcons)
		},

		getDataFromCache() {
			this.mainData = JSON.parse(localStorage.getItem("localMainData"))
			this.colors = JSON.parse(localStorage.getItem("localColors"))
			this.icons = JSON.parse(localStorage.getItem("localIcons"))
		},

		getColors() {
			axios
				.get(api.color.index, this.access_header)
				.then(response => {
					this.colors = response.data;
					this.$emit('onColorLoaded')
				})
				.catch(function (error) {console.log(error)});
		},

		getIcons() {
			axios
				.get(api.icon.index, this.access_header)
				.then(response => {
					this.icons = response.data;
					this.$emit('onIconsLoaded')
				})
				.catch(function (error) {console.log(error);});
		},

		saveProject(newProject) {
			let params = setUrlSearchParams(newProject);

			axios
				.post(api.project.create, params, this.access_header)
				.then(response => {
					this.updateData();
				})
				.catch(e=>{
					console.log('saveProject error: ', e);
					this.simpleConnectionCheck()
				})
		},

		deleteProject(id) {
			console.log(id)
			let params = new URLSearchParams();
				params.append('id', id)
			axios.get(api.project.delete + '?id='+ id, this.access_header)
				.then(response=>{
					this.updateData();
				})
				.catch( e=>{
					console.log('deleteProject error: ', e);
					this.simpleConnectionCheck();
				} )
		},

		saveService(newService) {
			let params = setUrlSearchParams(newService);
			axios
				.post(api.group.create, params, this.access_header)
				.then(response=>{
					console.log('-------', response)
					this.updateData()
				})
				.catch(e=>{
					console.log('saveService error: ', e)
					this.simpleConnectionCheck();
				})
		},

		updateService(changedService) {
			let params = setUrlSearchParams(changedService);
			axios
				.post(api.group.update + '?id=' + changedService.id, params, this.access_header)
				.then(response=>{
					this.updateData()
				})
				.catch(e=>{
					console.log('updateData error: ', e)
					this.simpleConnectionCheck();
				})
		},

		deleteService(group_id) {
			axios
				.get(api.group.delete + '?id=' + group_id, this.access_header)
				.then(response=>{
					this.updateData()
				})
				.catch(e=>{
					console.log('deleteService error: ', e)
					this.simpleConnectionCheck();
				})
		}
	},
})

function setUrlSearchParams(obj) {
	let params = new URLSearchParams();
	for (let key in obj) {
		if (!obj.hasOwnProperty(key)) continue;
		params.append(key, obj[key])
	}
	return params;
}


export {main, Search};