let url = 'https://storage.nova-solutio.com/api';

let api = {
	project: {
		index: 		url + '/project/' + 'index',
		view: 		url + '/project/' + 'view',
		delete: 	url + '/project/' + 'delete',
		update: 	url + '/project/' + 'update',
		create: 	url + '/project/' + 'create'
	},
	group: {
		index: 		url + '/group/' + 'index',
		view: 		url + '/group/' + 'view',
		delete: 	url + '/group/' + 'delete',
		update: 	url + '/group/' + 'update',
		create: 	url + '/group/' + 'create'
	},
	color: {
		index: 		url + '/color/' + 'index',
		view: 		url + '/color/' + 'view',
	},
	icon: {
		index: 		url + '/icon/' 	+ 'index'
	},
	user: {
		login: 		url + '/user/' + 'login',
	}
}

export default api;