import HomePage from './pages/home.vue';
import NotFoundPage from './pages/not-found.vue';
import PanelLeftPage from './pages/panel-left.vue';
import ProjectItem from './pages/project.vue';
import ServicePage from './pages/service-page.vue';
import ProjectEditor from './pages/project-editor.vue';
import NewService from './pages/new-service.vue';
import EditService from './pages/edit-service.vue';

export default [
	{
		path: '/',
		component: HomePage,
	},
	{
		path: '/project/:id',
		component: ProjectItem,
	},
	{
		path: '/project-editor/:project_id',
		component: ProjectEditor,
	},
	{
		path: '/service/:project_id/:service_id',
		component: ServicePage,
	},
	{
		path: '/new-service/:project_id',
		component: NewService,
	},
	{
		path: '/edit-service/:project_id/:service_id',
		component: EditService,
	},
	{
		path: '/panel-left/',
		component: PanelLeftPage,
	},
	{
		path: '(.*)',
		component: NotFoundPage,
	}
 
];
